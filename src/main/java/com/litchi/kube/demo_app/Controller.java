package com.litchi.kube.demo_app;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @author wanjiang
 * @version 1.0
 * @description: TODO
 * @date 2021/11/18 14:27
 */
@RestController
@RequestMapping("/hello")
public class Controller {

    @GetMapping
    public String hello() {
        if (System.getenv("COMPUTERNAME") != null)
            return "hello " + System.getenv("COMPUTERNAME");
        String hostname = null;
        try {
            hostname = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return "hello " + hostname;
    }
}
